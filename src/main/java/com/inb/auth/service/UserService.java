package com.inb.auth.service;

import com.inb.auth.entity.UserDetails;
import com.inb.auth.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    public void saveUser(UserDetails userDetails){
        userRepository.save(userDetails);
    }

    public UserDetails getUser(int userId){
        return userRepository.findById(userId).get();
    }

    public void deleteUser(int userId){
         userRepository.deleteById(userId);
    }

    public UserDetails updateUser(int userId, UserDetails userDetails){
        userDetails.setUserId(userId);
        userRepository.save(userDetails);
        return userDetails;
    }

}
