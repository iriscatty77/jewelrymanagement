package com.inb.auth.service;

import com.inb.auth.entity.Jewelry;
import com.inb.auth.repository.JewelryRepository;
import com.inb.auth.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class JewelryService {
    @Autowired
    private JewelryRepository jewelryRepository;

    @Autowired
    private UserRepository userRepository;

    public List<Jewelry> getJewelryByUser(int userId){
        return jewelryRepository.findByUserUserId(userId).get();
    }

    public List<Jewelry> addJewelryByUser(int userId, Jewelry jewelry){
        List<Jewelry> list = jewelryRepository.findByUserUserId(userId).get();
        jewelry.setUser(userRepository.findById(userId).get());
        list.add(jewelry);
        jewelryRepository.save(jewelry);
        return list;
    }

    public void deleteJewelry(int jewelryId){
        jewelryRepository.deleteById(jewelryId);
    }

    public Jewelry updateJewelry(int jewelryId, Jewelry jewelry) {
        jewelry.setJewelryId(jewelryId);
        jewelryRepository.save(jewelry);
        return jewelry;
    }
}
