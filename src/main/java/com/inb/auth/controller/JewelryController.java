package com.inb.auth.controller;

import com.inb.auth.entity.Jewelry;
import com.inb.auth.entity.UserDetails;
import com.inb.auth.service.JewelryService;
import com.inb.auth.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
public class JewelryController {

    @Autowired
    private JewelryService jewelryService;




    @RequestMapping(value="/users/{userId}/jewelry", method= RequestMethod.GET)
    public List<Jewelry> getAllJewelry(@PathVariable Integer userId){
        return jewelryService.getJewelryByUser(userId);
    }


    @RequestMapping(value="/users/{userId}/jewelry", method= RequestMethod.POST)
    public List<Jewelry> addJewelryByUser(@PathVariable Integer userId, @RequestBody Jewelry jewelry){
        return jewelryService.addJewelryByUser(userId, jewelry);
    }

    @RequestMapping(value="/jewelry/{jewelryId}", method= RequestMethod.DELETE)
    public void deleteJewelry(@PathVariable Integer jewelryId){
        jewelryService.deleteJewelry(jewelryId);
    }

    @RequestMapping(value = "jewelry/{jewelryId}",method= RequestMethod.PUT)
    public Jewelry updateJewelry(@PathVariable int jewelryId, @RequestBody Jewelry jewelry){
        jewelryService.updateJewelry(jewelryId, jewelry);
        return jewelry;
    }
}
