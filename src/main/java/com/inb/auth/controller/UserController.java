package com.inb.auth.controller;

import com.inb.auth.entity.UserDetails;
import com.inb.auth.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(value="/user",produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value="/idiot")
    public String welcome(){
        return "welcome boya";
    }

    @RequestMapping(method= RequestMethod.POST)
    public UserDetails createUser(@RequestBody UserDetails userDetails){
        userService.saveUser(userDetails);
        return userDetails;
    }

    @RequestMapping(value = "/{userId}", method= RequestMethod.GET)
    public UserDetails getUser(@PathVariable int userId){
        return userService.getUser(userId);
    }

    @RequestMapping(value = "/{userId}", method= RequestMethod.DELETE)
    public void deleteUser(@PathVariable int userId){
        userService.deleteUser(userId);
    }

    @RequestMapping(value = "/{userId}",method= RequestMethod.PUT)
    public UserDetails updateUser(@PathVariable int userId, @RequestBody UserDetails userDetails){
        userService.updateUser(userId, userDetails);
        return userDetails;
    }
}
