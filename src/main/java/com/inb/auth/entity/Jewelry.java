package com.inb.auth.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.inb.auth.entity.enums.JewelryBrand;
import com.inb.auth.entity.enums.JewelryGemstone;
import com.inb.auth.entity.enums.JewelryMetal;
import com.inb.auth.entity.enums.JewelryType;

import javax.persistence.*;

@Entity
public class Jewelry {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int jewelryId;


    
    private String name;
    private JewelryType jewelryType;
    private JewelryMetal jewelryMetal;
    private JewelryGemstone jewelryGemstone;
    private JewelryBrand jewelryBrand;
    private String designer;

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "user_id")
    private UserDetails user;

    public int getJewelryId() {
        return jewelryId;
    }

    public void setJewelryId(int jewelryId) {
        this.jewelryId = jewelryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public JewelryType getJewelryType() {
        return jewelryType;
    }

    public void setJewelryType(JewelryType jewelryType) {
        this.jewelryType = jewelryType;
    }

    public JewelryMetal getJewelryMetal() {
        return jewelryMetal;
    }

    public void setJewelryMetal(JewelryMetal jewelryMetal) {
        this.jewelryMetal = jewelryMetal;
    }

    public JewelryGemstone getJewelryGemstone() {
        return jewelryGemstone;
    }

    public void setJewelryGemstone(JewelryGemstone jewelryGemstone) {
        this.jewelryGemstone = jewelryGemstone;
    }

    public JewelryBrand getJewelryBrand() {
        return jewelryBrand;
    }

    public void setJewelryBrand(JewelryBrand jewelryBrand) {
        this.jewelryBrand = jewelryBrand;
    }

    public String getDesigner() {
        return designer;
    }

    public void setDesigner(String designer) {
        this.designer = designer;
    }

    public UserDetails getUser() {
        return user;
    }

    public void setUser(UserDetails user) {
        this.user = user;
    }
}
