package com.inb.auth.entity.enums;

public enum JewelryMetal {

    YellowGold_18k,
    YellowGold_14k,
    WhiteGold_18k,
    WhiteGold_14k,
    RoseGold_18k,
    RoseGold_14k,
    Platinum,
    SterlingSilver,
    Rodium,
    Other

}
