package com.inb.auth.entity.enums;

public enum JewelryBrand {

    Tiffany,
    VCA,
    StarJewelry,
    Tasaki,
    BlueNile,
    BonyLevy,
    Agatha,
    Swarovski,
    Other

}
