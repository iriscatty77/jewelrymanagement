package com.inb.auth.entity.enums;

public enum JewelryType {

    Necklace,
    Pendant,
    Bracelet,
    Earring,
    Ring

}
