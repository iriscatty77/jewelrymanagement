package com.inb.auth.entity.enums;

public enum JewelryGemstone {

    Diamond,
    Ruby,
    Emerald,
    Sapphire,
    Pearl,
    Moonstone,
    MotherOfPearl,
    Onyx,
    Carnelian,
    TigerEye,
    MalaChite,
    Crystal,
    Other

}
