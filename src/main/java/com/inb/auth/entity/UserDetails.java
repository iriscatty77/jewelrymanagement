package com.inb.auth.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.List;

@Entity
public class UserDetails {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private int userId;
    @Column
    private String name;

    private String company;

    private String address;

    private String email;

    private String phoneNumber;

    @OneToMany(cascade = CascadeType.ALL)
    @JsonManagedReference
    @JoinColumn(name = "user_id")
    private List<Jewelry> jewelryList;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public List<Jewelry> getJewelryList() {
        return jewelryList;
    }

    public void setJewelryList(List<Jewelry> jewelryList) {
        this.jewelryList = jewelryList;
    }
}
