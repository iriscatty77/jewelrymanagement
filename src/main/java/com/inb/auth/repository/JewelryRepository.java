package com.inb.auth.repository;

import com.inb.auth.entity.Jewelry;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface JewelryRepository extends JpaRepository<Jewelry, Integer> {

    Optional<List<Jewelry>> findByUserUserId(Integer integer);
}
