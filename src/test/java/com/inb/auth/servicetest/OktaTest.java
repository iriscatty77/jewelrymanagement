package com.inb.auth.servicetest;

import com.fasterxml.jackson.databind.JsonNode;
import com.inb.auth.requestentity.OktaUser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OktaTest {

    @Value("${okta.apikey}")
    private String apiKey;

    @Test
    public void testOkta(){

        RestTemplate restTemplate = new RestTemplate();
        String fooResourceUrl = "https://dev-203697.oktapreview.com/api/v1/users?limit=25";

        HttpHeaders headers = new HttpHeaders();

        headers.add("Authorization", "SSWS " + apiKey);
        //headers.set("user-key", "your-password-123"); // optional - in case you auth in headers

        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
        ResponseEntity<OktaUser[]> respEntity = restTemplate.exchange(fooResourceUrl, HttpMethod.GET, entity, OktaUser[].class);

        System.out.println("testing");
        System.out.println(respEntity.getBody()[0]);
    }
}
